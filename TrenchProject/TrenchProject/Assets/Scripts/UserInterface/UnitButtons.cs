﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitButtons : MonoBehaviour {

    public Sprite image;
    public Sprite image2;
    private Vector2 nextButtonPos;
    public float spacing = 5f;
    public Vector2 buttonSize = new Vector2(100,100);
    private RectTransform panelRect;

    float sX, sY;

    private void Awake()
    {
        panelRect = GetComponent<RectTransform>();
        sX = -panelRect.sizeDelta.x / 2 + buttonSize.x / 2 + spacing;
        sY = panelRect.sizeDelta.y / 2 - buttonSize.y / 2 - spacing;
        Vector2 startPos = new Vector2(sX , sY);
        nextButtonPos = startPos;

        CreateButton(image, "Test", Test);

    }

    public void CreateButton(Sprite image, string name, Action<int> callback)
    {
        GameObject buttonGO = new GameObject("_Button_" + name);
        RectTransform buttonRT = buttonGO.AddComponent<RectTransform>();
        Button buttonBU = buttonGO.AddComponent<Button>();
        Image buttonIM = buttonGO.AddComponent<Image>();

        buttonIM.sprite = image;
        buttonRT.SetParent(this.transform);

        buttonRT.localPosition = nextButtonPos;
        nextButtonPos = FindNextButtonPos(nextButtonPos);

        buttonRT.sizeDelta = buttonSize;


        buttonBU.onClick.AddListener(() => {
            buttonIM.sprite = image2;
            callback(1);
        });

    }
    private Vector2 FindNextButtonPos(Vector2 currentPos)
    {
        float x = currentPos.x, y = currentPos.y;
        if(x + buttonSize.x / 2 + spacing > panelRect.sizeDelta.x / 2)
        {
            x = sX;
            y -= buttonSize.y  + spacing;
        }
        else
        {
            x += buttonSize.x  + spacing;
        }

        return new Vector2(x, y);
    }


    void Test(int a)
    {
        print(a);
    }
}
