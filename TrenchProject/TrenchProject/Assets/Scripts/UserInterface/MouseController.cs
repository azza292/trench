﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour {

    public Sprite trenchDecal;

    private Vector2 mouseScreenPos;

    public GameObject CursorPrefab;
    private GameObject buildCursor;

    public bool ToggleCursor;
    public Sprite CursorHover;
    public Sprite CursorSelect;
    [Range(1f, 4f)]
    public float CursorDamping = 2f;

    [Range (5f, 30f)]
    public float PanSpeed = 15f;

    [Range(2f, 7f)]
    public float ScrollSpeed = 3f;
    public float MinScroll = 2f;
    public float MaxScroll = 8f;


    public float CamDamping = 5f;

    private SpriteRenderer cursorRender;
    private Vector3 mouseStartPos;

    private Camera mainCamera;
    private Vector3 newCamPos;

    private Troop troop;

    // Use this for initialization
    void Start () {
        mainCamera = Camera.main;
        buildCursor = Instantiate(CursorPrefab, Vector2.zero, new Quaternion(0,0,0,0), this.transform);
        cursorRender = buildCursor.GetComponent<SpriteRenderer>();
        cursorRender.sprite = CursorHover;
        buildCursor.SetActive(false);
    }
    private void Awake()
    {
        if(ToggleCursor)
        {
            Cursor.visible = false;
        }

        troop = FindObjectOfType<Troop>();
    }
    // Update is called once per frame
    void Update () {

        mouseScreenPos = Input.mousePosition;

        //If mouse in on screen
        if (mouseScreenPos.x > 0 && mouseScreenPos.y > 0 && mouseScreenPos.x < Screen.width && mouseScreenPos.y < Screen.height)
        {
            PanCamera();
            BuildTrenchs(mouseScreenPos);
            if (Input.GetButtonDown("Fire3"))
            {
                SetPosition(mouseScreenPos);
            }  
        }
            
    }

    void SetPosition(Vector3 mouseScreenPos)
    {
        Vector2 mouseWorldPos = mainCamera.ScreenToWorldPoint(mouseScreenPos);
        Tile currentTile = GroundTileManager.TileFromWorldPoint(mouseWorldPos);
        if (currentTile != null)
        {
            troop.SetTarget(currentTile.WorldPos);
        }
    }

    void PanCamera()
    {
        newCamPos = mainCamera.transform.position;

        if (Input.GetKey("w"))
        {
            newCamPos.y += PanSpeed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            newCamPos.x -= PanSpeed * Time.deltaTime;
        }
        if (Input.GetKey("s"))
        {
            newCamPos.y -= PanSpeed * Time.deltaTime;
        }
        if (Input.GetKey("d"))
        {
            newCamPos.x += PanSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        mainCamera.orthographicSize -= scroll * ScrollSpeed *50f * Time.deltaTime;
        mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, MinScroll, MaxScroll);

        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, newCamPos, CamDamping * Time.deltaTime);
    }
    void BuildTrenchs(Vector3 mouseScreenPos)
    {
        Vector2 mouseWorldPos = mainCamera.ScreenToWorldPoint(mouseScreenPos);
        Tile currentTile = GroundTileManager.TileFromWorldPoint(mouseWorldPos);

        if (currentTile != null)
        {
            buildCursor.SetActive(true);

            buildCursor.transform.position =  Vector3.Lerp(buildCursor.transform.position, currentTile.transform.position, CursorDamping * 10f * Time.deltaTime);

            if (Input.GetButton("Fire1"))
            {
                currentTile.Dig();
            }
            if(Input.GetButton("Fire2"))
            {
                currentTile.Remove();
            }
            else
            {
                cursorRender.sprite = CursorHover;
            }

        }
        else
        {
            buildCursor.SetActive(false);
        }
        
    }
}
