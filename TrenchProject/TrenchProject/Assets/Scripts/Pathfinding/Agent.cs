﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent : MonoBehaviour {


    public float MaximumVelocity = 1.5f;

    public Transform Tran;
    protected Steering steering;

    //Pathfinding
    public float WaypointBufferSize = 0.15f;
    public Vector2 CurrentWaypoint;
    int targetIndex;
    Vector2[] path;

    protected const string PathCoroutine = "FollowPath";
    
    public virtual void Awake()
    {
        Tran = GetComponent<Transform>();
        steering = gameObject.GetComponent<Steering>();

    }

    /// <summary>
    /// Requests a path to this target
    /// </summary>
    /// <param name="target">Target Position</param>
    /// <param name="callback">CallBack</param>
    public void SetTarget(Vector2 target)
    {
        if ((target == (Vector2)Tran.position))
        {
            Vector2[] newPath = new Vector2[] {target};
            PathSuccess(newPath);
        }
        else
        {
            PathRequestManager.RequestPath(Tran.position, target, OnPathEvaluated);
        }
    }

    public void OnPathEvaluated(Vector2[] newPath, bool pathSuccessful)
    {
        if(pathSuccessful)
        {
            PathSuccess(newPath);
        }
        else
        {
            PathFailure();
        }
    }

    private void PathSuccess(Vector2[] newPath)
    {
        Moving(true);
        path = newPath;
        StopCoroutine(PathCoroutine);
        StartCoroutine(PathCoroutine);
    }

    protected virtual void PathFailure()
    {
        Debug.Log("Path Not Found");
    }

    /// <summary>
    /// This is called once the end of path has been reached
    /// </summary>
    private void CompletedPath()
    {
        Moving(false);
        path = new Vector2[0];
        Tran.position = CurrentWaypoint;
    }

    /// <summary>
    /// Recalulate the Path
    /// </summary>
    public void ReCalculatePath()
    {
        Moving(false);
        SetTarget(path[path.Length-1]);
    }

    public virtual void Moving(bool s)
    {
        // Used to toggle moving modes on children
        // Could be better ?
    }

    /// <summary>
    /// The Co-Routine that traverses the path
    /// </summary>
    /// <returns></returns>
    IEnumerator FollowPath()
    {
        CurrentWaypoint = path[0];
        targetIndex = 0;
        bool exitCondition = false;
        while(!exitCondition)
        {
            Rect waypointBuffer = new Rect(
                CurrentWaypoint.x - WaypointBufferSize / 2,
                CurrentWaypoint.y - WaypointBufferSize / 2,
                WaypointBufferSize, 
                WaypointBufferSize);

            if (waypointBuffer.Contains(Tran.position))
            {
                targetIndex++;
                if(targetIndex >= path.Length)
                {
                    CompletedPath();
                    yield break;

                }
                CurrentWaypoint = path[targetIndex];
            }

            //Calculate Distance to next waypoint


            if (steering != null)
            {
                if(steering.LocalAvoidance())
                {
                    ReCalculatePath();
                    exitCondition = true;
                }

                steering.Steer();
            }
            else
            {
                Vector2 vectorToTarget = CurrentWaypoint - (Vector2)Tran.position;
                Tran.position += (Vector3)vectorToTarget.normalized * Time.deltaTime * MaximumVelocity;
            }

            
            yield return null;
        }
    }



    public virtual void OnDrawGizmos()
    {
        Vector3 cubeSize = Vector3.one / 4;

        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {

                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], cubeSize);

                if (i == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[i]);
                }
                else
                {
                    Gizmos.DrawLine(path[i - 1], path[i]);
                }
            }
        }
    }

}
