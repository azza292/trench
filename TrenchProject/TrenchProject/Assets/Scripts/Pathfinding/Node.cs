﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : iHeapItem<Node>{

    public int X;
    public int Y;
    public bool Walkable;
    public int MovementPenalty;
    public Vector2 WorldPos;

    // AI
    public Node parent;
    public int gCost;
    public int hCost;
    int heapIndex;


    public Node(int x, int y, bool walkable,int penalty, Vector2 worldPos)
    {
        this.X = x;
        this.Y = y;
        this.Walkable = walkable;
        this.MovementPenalty = penalty;
        this.WorldPos = worldPos;
    }
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }

    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }
        set
        {
            heapIndex = value;
        }
    }

    public int CompareTo(Node nodeToCompare)
    {
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        if(compare == 0)
        {
            compare = hCost.CompareTo(nodeToCompare.hCost);
        }
        return -compare;
    }

}
