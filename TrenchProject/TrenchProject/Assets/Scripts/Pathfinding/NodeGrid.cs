﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGrid {

    public static NodeGrid Instance
    {
        get
        {
            return instance;
        }
    }
    private static NodeGrid instance;

    private Node[,] WorldNodes;

    //Returns total map size
    public int MaxSize
    {
        get
        {
            return (int)mapSize.x * (int)mapSize.y;
        }
    }
    public bool NodesGenerated;

    private Vector2 mapSize;

    public NodeGrid(Vector2 mapSize)
    {
        this.mapSize = mapSize;
        instance = this;

        WorldNodes = new Node[(int)mapSize.x, (int)mapSize.y];
    }

    /// <summary>
    /// Generate a node
    /// </summary>
    public void GenerateNode(int x, int y, bool walkable, int movementPenalty, Vector2 worldPos)
    {
        WorldNodes[x, y] = new Node(x, y, walkable, movementPenalty, worldPos);
    }

    // This could be done better - Ahh fuck it she'll be right mate
    public Node NodeFromWorldPoint(Vector2 worldPoint)
    {
        Tile currentTile = GroundTileManager.TileFromWorldPoint(worldPoint);
        if (currentTile != null)
        {
            return WorldNodes[currentTile.X, currentTile.Y];
        }
        return null;
    }

    public void UpdateNode(int x, int y, bool walkable, int movementPenalty)
    {
        WorldNodes[x, y].Walkable = walkable;
        WorldNodes[x, y].MovementPenalty = movementPenalty;
    }

    public List<Node> GetNeighbors(Node cNode)
    {
        List<Node> neighbors = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                int checkX = cNode.X + x;
                int checkY = cNode.Y + y;

                if (checkX >= 0 && checkX < mapSize.x && checkY >= 0 && checkY < mapSize.y)
                {
                    neighbors.Add(WorldNodes[checkX, checkY]);
                }
            }
        }
        return neighbors;
    }

}
