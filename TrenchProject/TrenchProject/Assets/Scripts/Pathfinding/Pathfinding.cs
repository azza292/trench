﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

public class Pathfinding : MonoBehaviour {

    PathRequestManager requestManager;
    NodeGrid nodeGrid;

    public void StartFindPath(Vector2 startPos, Vector2 targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }

    private void Awake()
    {
        requestManager = GetComponent<PathRequestManager>();
        nodeGrid = NodeGrid.Instance;
    }

    IEnumerator FindPath(Vector2 startPos, Vector2 targetPos)
    {
        Vector2[] wayPoints = new Vector2[0];
        bool pathSuccess = false;

        Node startNode = nodeGrid.NodeFromWorldPoint(startPos);
        Node targetNode = nodeGrid.NodeFromWorldPoint(targetPos);


        if (startNode.Walkable && targetNode.Walkable && (startNode != targetNode))
        {
            Heap<Node> openSet = new Heap<Node>(nodeGrid.MaxSize);
            HashSet<Node> closedSet = new HashSet<Node>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                Node currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    pathSuccess = true;
                    break;
                }

                foreach (Node neighbour in nodeGrid.GetNeighbors(currentNode))
                {
                    if (!neighbour.Walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newMovementCostToNeighbor = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.MovementPenalty;
                    if (newMovementCostToNeighbor < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newMovementCostToNeighbor;
                        neighbour.hCost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                        else
                        {
                            openSet.UpdateItem(neighbour);
                        }
                    }
                }
            }
        }


        yield return null;

        if (pathSuccess)
        {
            wayPoints = RetracePath(startNode, targetNode);
        }

        requestManager.FinishedProcessingPath(wayPoints, pathSuccess);
    }


    Vector2[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while(currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Add(startNode);
        Vector2[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);
        
        return waypoints;        
    }

    /// <summary>
    /// Returns an array of points, adds only points where direction changes
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    Vector2[] SimplifyPath(List<Node> path) {

        List<Vector2> waypoints = new List<Vector2>();
        Vector2 directionOld = Vector2.zero;


        for(int i = 1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].X - path[i].X, path[i - 1].Y - path[i].Y);
            if(directionNew != directionOld)
            {
                waypoints.Add(path[i-1].WorldPos);
            }
            directionOld = directionNew;
        }

        return waypoints.ToArray();
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int distX = Mathf.Abs(nodeA.X - nodeB.X);
        int distY = Mathf.Abs(nodeA.Y - nodeB.Y);

        return Mathf.Min(distX, distY) * 14 + Mathf.Abs(distX - distY) * 10;
    }
}
