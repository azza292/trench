﻿
using UnityEngine;

public class Steering : MonoBehaviour {

    public float MaxTurn = 5f;
    public float TurnSpeed = 10f;
    public float LookAheadDist = 0.2f;



    Vector2 currentVelocity;
    Vector2 vectorToTarget;
    Vector2 acceleration = new Vector2(0, 0);

    Agent agent;

	// Use this for initialization
	void Start () {
        agent = GetComponent<Agent>();
	}
	
    public void Steer()
    {
        vectorToTarget = agent.CurrentWaypoint - (Vector2)agent.Tran.position;

        CalculateRotation();
        Seek();

        currentVelocity += acceleration;
        currentVelocity = Vector2.ClampMagnitude(currentVelocity, agent.MaximumVelocity);
        agent.Tran.position += (Vector3)currentVelocity * Time.deltaTime;
        acceleration = acceleration * 0;
    }

    void CalculateRotation()
    {
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        agent.Tran.rotation = Quaternion.Slerp(agent.Tran.rotation, q, Time.deltaTime * TurnSpeed);
    }


    /// <summary>
    /// Seek Behavior
    /// </summary>
    /// <param name="target"></param>
    void Seek()
    {
        Vector2 vectorToTargetNormal = vectorToTarget.normalized;

        vectorToTargetNormal = vectorToTargetNormal * agent.MaximumVelocity;

        Vector2 steering = vectorToTargetNormal - currentVelocity;

        steering = Vector2.ClampMagnitude(steering, MaxTurn / 10f);


        ApplyForce(steering);

    }

    void ApplyForce(Vector2 force)
    {
        acceleration += force;
    }

    /// <summary>
    /// Checks points between the Unit and the next waypoint
    /// </summary>
    /// <returns>returns True of object is in the way</returns>
    public bool LocalAvoidance()
    {

        //Direction to next waypoint
        Vector3 directionToTarget = Vector3.Normalize(vectorToTarget);
        float radius = 0.1f;

        //Look Ahead Halfway to next waypoint
        Vector2 halfwayPoint = agent.Tran.position + (Vector3)vectorToTarget / 2;
        if (!PointWalkable(halfwayPoint, radius))
        {
            return true;
        }

        // Look Directly infront of Unit
        Vector2 stepAhead = agent.Tran.position + directionToTarget * LookAheadDist;
        if (!PointWalkable(stepAhead, radius))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Checks points within a circle and return a bool determining if the tile contained is walkable
    /// </summary>
    bool PointWalkable(Vector2 origin, float radius)
    {
        RaycastHit2D[] hitInfos = Physics2D.CircleCastAll(origin, radius, Vector2.zero);
        foreach (RaycastHit2D hit in hitInfos)
        {
            if (hit.collider != null && hit.collider.gameObject.GetComponent<Tile>() as Tile != null)
            {
                Tile tile = hit.collider.gameObject.GetComponent<Tile>();
                tile.Test();
                if (tile.Walkable)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
