﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Troop : Agent {

    private Animator anim;

    public override void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        base.Awake();
    }

    public override void Moving(bool s)
    {
        if (anim != null)
        {
            anim.SetBool("walking", s);
        }
        base.Moving(s);
    }
}
