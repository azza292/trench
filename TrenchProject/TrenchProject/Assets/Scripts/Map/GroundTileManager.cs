﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTileManager : MonoBehaviour
{


    [Header("Constants")]
    public GameObject TileParentObject;
    public GameObject TilePrefab;

    [HideInInspector]
    // Put sprites in a spite manager
    public Sprite[] SpriteTiles;

    private Tile[,] WorldMap;
    private float tileSize;
    private List<Tile> activeMap = new List<Tile>();

    public LayerMask layerMask;
    private static LayerMask sLayerMask;

    private NodeGrid nodeGrid;


    [Header("Options")]
    public bool LiveView = false;
    public Vector2 MapSize;
    private Vector2 newPos;


    /// <summary>
    /// Dictionary lookup to convert possible IDs to 42
    /// </summary>
    private Dictionary<int, int> tileDict =  new Dictionary<int, int>
    {
    { 2, 1 }, { 8 , 2 }, { 10 , 3 }, { 11 , 4}, { 16 , 5}, { 18 , 6}, { 22 , 7}, { 24 , 8}, { 26 , 9}, { 27 , 10}, { 30 , 11}, { 31 , 12}, { 64 , 13}, { 66 , 14}, { 72 , 15}, { 74 , 16}, { 75 , 17},{ 80 , 18},{ 82 , 19}, { 86 , 20},{ 88 , 21} , { 90 , 22}, { 91 , 23}, { 94 , 24}, { 95 , 25}, { 104 , 26}, { 106 , 27}, { 107 , 28}, { 120 , 29}, { 122 , 30}, { 123 , 31}, { 126 , 32}, { 127 , 33}, { 208 , 34}, { 210 , 35}, { 214 , 36}, { 216 , 37}, { 218 , 38}, { 219 , 39}, { 222 , 40}, { 223 , 41}, { 248 , 42}, { 250 , 43}, { 251 , 44}, { 254 , 45}, { 255 , 46}, { 0 , 47 }
    };

    /// <summary>
    /// Remove single units when removing trenchs
    /// </summary>
    private int[] idRemover = new int[] { 1, 2, 3, 5, 6, 8, 9, 13, 14, 15, 16, 18, 19, 21, 22, 47 };

    public enum TileIds
    {
        Dirt = 46,
        Null = -1
    }

    public enum MovementPenalty
    {
        Dirt = 0,
        Grass = 5000,
        TrenchWall = 5000
    }

    // Use this for initialization
    void Awake()
    {
        nodeGrid = new NodeGrid(MapSize);

        SpriteTiles = Resources.LoadAll<Sprite>("WorldTiles");
        tileSize = SpriteTiles[0].rect.size.x / 100;



        WorldMap = new Tile[(int)MapSize.x, (int)MapSize.y];

        GenerateMapGrid((int)TileIds.Null);


        sLayerMask = layerMask;

    }

    /// <summary>
    /// Populates the array of tiles and places them in the scene
    /// </summary>
    /// <param name="iD">Default Tile to place</param>
    void GenerateMapGrid(int defaultId)
    {
        newPos = TileParentObject.transform.position;
        for (int x = 0; x < MapSize.x; x++)
        {
            for (int y = 0; y < MapSize.y; y++)
            {
                Quaternion rot = new Quaternion(0, 0, 0, 0);

                GameObject clone = Instantiate(TilePrefab, newPos, rot, TileParentObject.transform);
                Tile cTile = clone.GetComponent<Tile>();

                WorldMap[x, y] = cTile;
                cTile.Setup(x, y, defaultId, this);

                nodeGrid.GenerateNode(x,y, cTile.Walkable, cTile.MovementPenalty, cTile.transform.position);

                newPos.y -= tileSize;
            }
            newPos.x += tileSize;
            newPos.y = TileParentObject.transform.position.y;
        }
    }

    /// <summary>
    /// Updates all the tiles AND nodes in the active array
    /// </summary>
    public void UpdateTiles()
    {
        foreach (Tile t in activeMap)
        {
            if (t.ID != (int)TileIds.Null)
            {
                t.UpdateID(BitIndex8(t));
                nodeGrid.UpdateNode(t.X, t.Y, t.Walkable, t.MovementPenalty);
            }
        }
    }

    /// <summary>
    /// Adds an individual tile to the active array
    /// </summary>
    /// <param name="cTile"></param>
    /// <param name="isNeighbor"></param>
    /// <returns></returns>
    public void AddTile(Tile cTile, bool isNeighbor)
    {
        if (isNeighbor)
        {
            // add current tile
            if (!(activeMap.Contains(cTile)))
            {
                activeMap.Add(cTile);
            }
        }
        else
        {
            // add current tile
            if (!(activeMap.Contains(cTile)))
            {
                activeMap.Add(cTile);
            }
            Neighbors(cTile, AddNeighbor);
        }
    }

    public void RemoveTile(Tile cTile, bool isNeighbor)
    {
        if (isNeighbor)
        {
            //Remove current tile
            if (activeMap.Contains(cTile))
            {
                activeMap.Remove(cTile);
            }
        }
        else
        {
            //remove current tile
            if (activeMap.Contains(cTile))
            {
                activeMap.Remove(cTile);
            }
            Neighbors(cTile, RemoveNeighbor);
        }
    }

    /// <summary>
    /// Gets Neighbors of active array and returns each Tile and location relative to cTile
    /// </summary>
    /// <param name="cTile"></param>
    private void Neighbors(Tile cTile, Action<Tile, int, int> callback)
    {
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }
                callback(cTile, x, y);
            }
        }
    }

    private void AddNeighbor(Tile cTile, int x, int y)
    {
        WorldMap[cTile.X + x, cTile.Y + y].Dig(true);
    }

    private void RemoveNeighbor(Tile cTile, int x, int y)
    {
        Tile temp = WorldMap[cTile.X + x, cTile.Y + y];
        if (temp.ID != (int)TileIds.Null)
        {
            //Remove the single line types
            for (int i = 0; i < idRemover.Length; i++)
            {
                if (temp.ID == idRemover[i])
                {
                    temp.Remove(true);
                    break;
                }
            }
        }
    }


    public bool WithinMap(Tile cTile)
    {
        if (cTile.X >= 3 && cTile.X <= MapSize.x - 4 && cTile.Y >= 3 && cTile.Y <= MapSize.y - 4)
        {
            return true;
        }
        return false;
    }

    // This could be done better - Ahh fuck it she'll be right mate
    public static Tile TileFromWorldPoint(Vector2 worldPoint)
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(worldPoint, Vector2.zero, 1f, sLayerMask);
        if (hitInfo.collider != null && hitInfo.collider.gameObject.GetComponent<Tile>() as Tile != null)
        {
            Tile cTile = hitInfo.collider.gameObject.GetComponent<Tile>();
            return cTile;
        }
        print("ERROR - Node not found from world point");
        return null;
    }

    public static Tile FindSappingPoint(Tile cTile)
    {
        List<Tile> list = new List<Tile>();
        RaycastHit2D[] hitInfos = Physics2D.BoxCastAll(cTile.WorldPos, Vector2.one / 2, 0, Vector2.zero);
        foreach (RaycastHit2D hit in hitInfos)
        {
            if (hit.collider != null && hit.collider.gameObject.GetComponent<Tile>() as Tile != null)
            {
                Tile newTile = hit.collider.gameObject.GetComponent<Tile>();
                if (newTile.ID == (int)GroundTileManager.TileIds.Dirt && newTile.Walkable == true)
                {
                    list.Add(newTile);
                }
            }
        }

        if (list.Count > 0)
        {
            Tile closestSapPoint = GetClosest(list, cTile.WorldPos);
            return closestSapPoint;
        }
        return null;

    }

    public static Tile GetClosest(List<Tile> list, Vector2 pos)
    {
        Tile closestTrench = list[0];
        Vector2 vector = (pos - closestTrench.WorldPos);
        float shortestDistance = vector.magnitude;
        float newDistance;
        foreach (Tile currentTile in list)
        {
            vector = (pos - currentTile.WorldPos);
            newDistance = vector.magnitude;
            if (newDistance < shortestDistance)
            {
                shortestDistance = newDistance;
                closestTrench = currentTile;
            }
        }
        return closestTrench;
    }

    /// <summary>
    /// Returns the correct index for the tile by checking all tiles around it
    /// </summary>
    /// <param name="cTile"></param>
    /// <returns></returns>
    private int BitIndex8(Tile cTile)
    {
        int x = cTile.X; int y = cTile.Y;

        int index, newIndex, north_tile, south_tile, west_tile, east_tile, north_east_tile, north_west_tile, south_east_tile, south_west_tile;
        // Directional check

        north_tile = WorldMap[x, y - 1].IsBlock();
        east_tile = WorldMap[x + 1, y].IsBlock();
        south_tile = WorldMap[x, y + 1].IsBlock();
        west_tile = WorldMap[x - 1, y].IsBlock();


        north_west_tile = WorldMap[x - 1, y - 1].IsBlock() & north_tile & west_tile;
        north_east_tile = WorldMap[x + 1, y - 1].IsBlock() & north_tile & east_tile;
        south_west_tile = WorldMap[x - 1, y + 1].IsBlock() & south_tile & west_tile;
        south_east_tile = WorldMap[x + 1, y + 1].IsBlock() & south_tile & east_tile;

        //8 bit Bitmasking calculation using Directional check booleans values
        index = north_west_tile + (2 * north_tile) + (4 * north_east_tile) + (8 * west_tile) + (16 * east_tile) + (32 * south_west_tile) + (64 * south_tile) + (128 * south_east_tile);

        // take the previously calculated value and find the relevant value in the data structure to remove redundancies
        if (tileDict.TryGetValue(index, out newIndex))
        {
            return newIndex;
        }
        else
        {
            throw new Exception("No index found!");
        }

    }
}
