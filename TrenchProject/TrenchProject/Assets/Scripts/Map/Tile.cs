﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tile : MonoBehaviour
{

    public int X;
    public int Y;

    public int ID
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }
    }
    private int id;

    public bool Walkable
    {
        get { return walkable; }
    }
    private bool walkable;

    public int MovementPenalty
    {
        get { return movementPenalty; }
    }
    private int movementPenalty;

    public Vector2 WorldPos {
        get { return transform.position;}
    }

    private SpriteRenderer spriteRender;
    private GroundTileManager tileManager;


    public void Setup(int x, int y, int id, GroundTileManager tileManager)
    {
        X = x;
        Y = y;
        this.tileManager = tileManager;
        spriteRender = GetComponent<SpriteRenderer>();
        UpdateID(id);

    }

    /// <summary>
    /// Checks if a tile block exists here
    /// </summary>
    /// <returns></returns>
    public int IsBlock()
    {
        if (ID != (int)GroundTileManager.TileIds.Null)
        {
            return 1;
        }
        return 0;
    }

    public void UpdateTile(int newId, int movementPenalty, bool walkable)
    {
        this.id = newId;
        this.movementPenalty = movementPenalty;
        this.walkable = walkable;
        UpdateSprite(newId);
    }

    private void UpdateSprite(int newId)
    {
        if (newId != (int)GroundTileManager.TileIds.Null)
        {
            spriteRender.sprite = tileManager.SpriteTiles[newId];
        }
        else
        {
            spriteRender.sprite = null;
        }
    }

    public void UpdateID(int newId)
    {
        id = newId;

        switch ((GroundTileManager.TileIds)newId)
        {
            case GroundTileManager.TileIds.Dirt:
                RemoveDecals();
                walkable = true;
                movementPenalty = (int)GroundTileManager.MovementPenalty.Dirt;
                break;
            case GroundTileManager.TileIds.Null:
                walkable = true;
                movementPenalty = (int)GroundTileManager.MovementPenalty.Grass;
                break;
            default:
                walkable = false;
                movementPenalty = (int)GroundTileManager.MovementPenalty.TrenchWall;
                break;
        }

        UpdateSprite(newId);
    }

    /// <summary>
    /// Updates the TILE in world MAP
    /// </summary>
    public void Dig(bool isNeighbor = false)
    {
        if (tileManager.WithinMap(this))
        {
            UpdateID((int)GroundTileManager.TileIds.Dirt);
            tileManager.AddTile(this, isNeighbor);
            tileManager.UpdateTiles();
        }
    }


    /// <summary>
    /// Removes TIlE from the world map
    /// </summary>
    public void Remove(bool isNeighbor = false)
    {
        if (tileManager.WithinMap(this))
        {
            UpdateID((int)GroundTileManager.TileIds.Null);
            tileManager.UpdateTiles();
            tileManager.RemoveTile(this, isNeighbor);
        }
    }

    public void AddDecal(Sprite s)
    {
        GameObject clone = new GameObject("Decal" + name);
        clone.transform.SetParent(GetComponent<Transform>());
        SpriteRenderer render = clone.AddComponent<SpriteRenderer>();
        render.sprite = s;
        render.sortingOrder = 1;
        clone.transform.position = transform.position;
    }

    public void RemoveDecals()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void Test()
    {
        spriteRender.color = Color.red;
    }

}